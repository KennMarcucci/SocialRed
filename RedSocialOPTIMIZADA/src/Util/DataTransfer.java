
package Util;


public class DataTransfer {

	private String nombreCompleto;
	private static DataTransfer instance;
	
	private DataTransfer() {		
	}
	
	public static DataTransfer getInstance() {
		if(instance==null) {
			instance = new DataTransfer();
		}
		
		return instance;
	}
	
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
}
