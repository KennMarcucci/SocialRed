
package Modelo;

import java.util.ArrayList;
import java.util.Calendar;

public class UsuarioDAO {
    
    public ArrayList<UsuarioDTO> listaUsuarios;
	
	public UsuarioDAO() {
		listaUsuarios = new ArrayList<UsuarioDTO>();
	}
	
	public void agregarUsuario(String nombre, String apellido, String usuario, String edad, String clave, String correo) {
        UsuarioDTO user = new UsuarioDTO();
        user.setNombre(nombre);
        user.setApellido(apellido);
        user.setCorreo(correo);
        user.setEdad(edad);
        user.setPassword(clave);
        user.setUsername(usuario);        
        listaUsuarios.add(user);
        }
        public void agregarUsuario(UsuarioDTO user) {               
        listaUsuarios.add(user);
        }
	
        public UsuarioDTO buscarUsuario(String usuario) {
            UsuarioDTO user=null;
            if (!listaUsuarios.isEmpty()) {
                for(UsuarioDTO t:listaUsuarios){
                    if(t.getUsername().equals(usuario)){
                        user=t;
                        break;
                    }
                }
                return user;
            }
            return user;
        }
        
        public boolean usuarioDuplicado(String usuario) {
        if (!usuario.isEmpty()) {
            for (UsuarioDTO user : listaUsuarios){
                if (user.getUsername().equals(usuario)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean validarUsuario(String usuario, String password) {
        if (!usuario.isEmpty()) {
            for (UsuarioDTO user : listaUsuarios){
                if (user.getUsername().equals(usuario)&&user.getPassword().equals(password)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public void agregarAmigo(String usuario,UsuarioDTO amigo) {
        buscarUsuario(usuario).addAmigo(amigo);
    }
    
    public boolean validarAmigo(String usuario,String amigo) {
        UsuarioDTO user = buscarUsuario(usuario);
        if (!user.getAmigos().isEmpty()) {
            for (UsuarioDTO amigos:user.getAmigos()) {
                if (amigos.getUsername().equals(amigo)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public UsuarioDTO buscarAmigos(String usuario,String amigo) {
        UsuarioDTO user = buscarUsuario(usuario);
        UsuarioDTO friend = null;
        if (!user.getAmigos().isEmpty()) {
            for (UsuarioDTO amigos:user.getAmigos()) {
                if (amigos.getUsername().equals(amigo)) {
                    friend=amigos;
                }
            }
        }
        return friend;
    }
        
    public ArrayList<UsuarioDTO> obtenerUsuarios(){
            return listaUsuarios;
    }
}
