
package Modelo;

import java.util.ArrayList;

public class UsuarioDTO {
    private String nombre;
    private String apellido;
    private String edad;
    private String correo;

    private String username;
    private String password;

    private ArrayList<UsuarioDTO> amigos;

    public UsuarioDTO(){
        amigos = new ArrayList<>();
    }
    
    public UsuarioDTO(String nombre, String apellido, String edad, String correo, String username, String password) {
        this();
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.correo = correo;
        this.username = username;
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<UsuarioDTO> getAmigos() {
        return amigos;
    }

    public void setAmigos(ArrayList<UsuarioDTO> amigos) {
        this.amigos = amigos;
    }

    public void addAmigo(UsuarioDTO amigo){
        this.amigos.add(amigo);
    }

    
}