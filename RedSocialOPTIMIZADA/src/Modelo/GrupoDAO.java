/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author marcu
 */
public class GrupoDAO {
    ArrayList<GrupoDTO> grupos = new ArrayList<>();
    ArrayList<UsuarioDTO> request = new ArrayList<>();

    public GrupoDAO() {
    }

    public ArrayList<UsuarioDTO> getRequest() {
        return request;
    }

    public void setRequest(ArrayList<UsuarioDTO> request) {
        this.request = request;
    }

    public ArrayList<GrupoDTO> getGrupos() {
        return grupos;
    }

    public void setGrupos(ArrayList<GrupoDTO> grupos) {
        this.grupos = grupos;
    }

    public void addGrupo(String nombre, String descripcion, String tipoGrupo, String noticiasRecientes, String usuario, UsuarioDTO miembro) {
        GrupoDTO grupodata= new GrupoDTO(nombre, descripcion, tipoGrupo, noticiasRecientes, usuario);
        grupodata.addMember(miembro);
        grupos.add(grupodata);       
    }

    public void addRequest(UsuarioDTO quiereEntrar) {
        request.add(quiereEntrar);
    }
}
