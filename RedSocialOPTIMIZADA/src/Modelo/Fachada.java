
package Modelo;

import java.util.ArrayList;

public class Fachada {

	// Patrón Singleton
	private static Fachada instance;
	private UsuarioDAO usuarioDAO;
	private PublicacionDAO publicacionDAO;
        private GrupoDAO grupoDAO;
	
	private Fachada() {
		usuarioDAO = new UsuarioDAO();
		publicacionDAO = new PublicacionDAO();
                grupoDAO = new GrupoDAO();
	}
	
	public static Fachada getInstance() {
		if(instance==null)
			instance = new Fachada();
		
		return instance;
	}
	
	public ArrayList<UsuarioDTO> listarUsuarios(){
		
		ArrayList<UsuarioDTO> listaUsuarios = null;
		
		if(usuarioDAO!=null)
			listaUsuarios = usuarioDAO.obtenerUsuarios();
		
		return listaUsuarios;
		
	}
	
	
	public ArrayList<PublicacionDTO> listarPublicacionesUsuario(String usuario){
	
		ArrayList<PublicacionDTO> listaPublicaciones = publicacionDAO.listarPublicacionesUsuario(usuario);
		return listaPublicaciones;
		
	}
	
	public void crearPublicacion(String nombres, String texto) {
		publicacionDAO.crearPublicacion(nombres, texto);
	}
	
}
