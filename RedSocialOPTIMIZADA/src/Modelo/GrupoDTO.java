/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author marcu
 */
public class GrupoDTO {

   private String nombre;
    private String descripcion;
    private String tipoGrupo;
    private String noticiasRecientes;
    private String usuario;
    
    private UsuarioDAO miembros;
    
    public GrupoDTO() {
        miembros = new UsuarioDAO();
    }

    public GrupoDTO(String nombre, String descripcion, String tipoGrupo, String noticiasRecientes, String usuario) {
        this();
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipoGrupo = tipoGrupo;
        this.noticiasRecientes = noticiasRecientes;
        this.usuario = usuario; 
    }

    public UsuarioDAO getMiembros() {
        return miembros;
    }

    public void setMiembros(UsuarioDAO miembros) {
        this.miembros = miembros;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoGrupo() {
        return tipoGrupo;
    }

    public void setTipoGrupo(String tipoGrupo) {
        this.tipoGrupo = tipoGrupo;
    }

    public String getNoticiasRecientes() {
        return noticiasRecientes;
    }

    public void setNoticiasRecientes(String noticiasRecientes) {
        this.noticiasRecientes = noticiasRecientes;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public void addMember(UsuarioDTO miembro){
        miembros.agregarUsuario(miembro);
    }
}
