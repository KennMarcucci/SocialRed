/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Modelo.GrupoDAO;
import Modelo.GrupoDTO;
import Modelo.UsuarioDAO;
import Modelo.UsuarioDTO;
import Vista.Login;

/**
 *
 * @author marcu
 */
public class Main {

    public static void main(String[] args) {
        Test test = new Test();
        UsuarioDAO userDAO = new UsuarioDAO();
        GrupoDAO grupoDAO = new GrupoDAO();
        
        userDAO = test.testiar();
        grupoDAO = test.testiarGrupo();
        

        Login lg = new Login();
        lg.setVisible(true);
    }
}
