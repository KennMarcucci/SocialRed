/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Modelo.*;

public class Test {

    public UsuarioDAO testiar() {
        UsuarioDAO usersDAO = new UsuarioDAO();

        UsuarioDTO userData1 = new UsuarioDTO("Admin El", "God", "999", "admin@gmail.com", "Admin", "Admin");
        usersDAO.agregarUsuario(userData1);

        UsuarioDTO userData2 = new UsuarioDTO("Kenn Alejandro", "Marcucci", "21", "kenn@gmail.com", "Kenn", "Kenn");
        usersDAO.agregarUsuario(userData2);

        UsuarioDTO userData3 = new UsuarioDTO("Wendy Mariana", "Monsalve", "20", "wendy@gmail.com", "Wendy", "Wendy");
        usersDAO.agregarUsuario(userData3);

        UsuarioDTO userData4 = new UsuarioDTO("Andres Giga", "Chad", "1", "andresrobloxprogamer@gmail.com", "Andres", "Andres");
        usersDAO.agregarUsuario(userData4);

        UsuarioDTO userData5 = new UsuarioDTO("Virginia", "Marcucci", "66", "virginia@gmail.com", "Virginia", "Virginia");
        usersDAO.agregarUsuario(userData5);

        UsuarioDTO userData6 = new UsuarioDTO("Diego Andres", "Trigos", "22", "diego@gmail.com", "Diego", "Diego");
        usersDAO.agregarUsuario(userData6);

        UsuarioDTO userData7 = new UsuarioDTO("Armando Pedrozo", "Villarial", "77", "armando@gmail.com", "Armando", "Armando");
        usersDAO.agregarUsuario(userData7);

        UsuarioDTO userData8 = new UsuarioDTO("Amparo Daza", "de Marcucci", "90", "amparo@gmail.com", "Amparo", "Amparo");
        usersDAO.agregarUsuario(userData8);

        UsuarioDTO userData9 = new UsuarioDTO("Jose Daniel", "Colmenares", "55", "jose@gmail.com", "Jose", "Jose");
        usersDAO.agregarUsuario(userData9);
        return usersDAO;
    }

    public GrupoDAO testiarGrupo() {
        GrupoDAO grupoDAO = new GrupoDAO();
        for (UsuarioDTO miembro : testiar().obtenerUsuarios()) {
            if (miembro.getUsername().equals("Kenn")) {
                grupoDAO.addGrupo("PooII", "Este es un grupo de PooII", "Informativo", "El martes nos revisan la redSocial xd", "Kenn", miembro);
            }
            if (miembro.getUsername().equals("Admin")) {
                grupoDAO.addGrupo("Razer", "Este es un grupo de Razer", "Compra/Venta", "100% de descuento en PC gamers", "Admin", miembro);
            }
        }
        return grupoDAO;
    }
}
