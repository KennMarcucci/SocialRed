/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author marcu
 */
public class Principal {

    ArrayList<Usuario> users = new ArrayList<>();
    ArrayList<Grupo> groups = new ArrayList<>();
    ArrayList<Usuario> amigos = new ArrayList<>();

    public void agregarGrupoEstudiante(String owner, String grupox) {
        for (Usuario usuario : users) {
            if (usuario.getUsername().equalsIgnoreCase(owner)) {
                for (Grupo grupo : groups) {
                    if (grupo.getNombre().equalsIgnoreCase(grupox)) {
                        usuario.agregarGrupo(grupo);
                    }
                }
            }
        }
    }

    public Usuario buscarUsuario(String usuario) {
        if (!users.isEmpty()) {
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).getUsername().equals(usuario)) {
                    return users.get(i);
                }
            }
        }
        return null;
    }

    public Usuario buscarAmigos(String usernameAmigo) {
        if (!amigos.isEmpty()) {
            for (int i = 0; i < amigos.size(); i++) {
                if (amigos.get(i).getUsername().equals(usernameAmigo)) {
                    return amigos.get(i);
                }
            }
        }
        return null;
    }

    public boolean validarAmigo(String usuario) {
        if (!amigos.isEmpty()) {
            for (int i = 0; i < amigos.size(); i++) {
                if (amigos.get(i).getUsername().equals(usuario)) {
                    return true;
                }
            }
        }
        return false;
    }

    public ArrayList<Usuario> getAmigos() {
        return amigos;
    }

    public void agregarAmigos(Usuario amigo) {
        Usuario user = new Usuario(amigo.getNombre(), amigo.getApellido(), amigo.getUsername(), amigo.getEdad(), amigo.getPassword(), amigo.getCorreo());
        amigos.add(user);

    }

    public boolean validarUsuario(String usuario, String password) {
        if (!users.isEmpty()) {
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).getUsername().equals(usuario) && users.get(i).getPassword().equals(password)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean usuarioDuplicado(String usuario) {
        if (!usuario.isEmpty()) {
            for (Usuario user : users) {
                if (user.getUsername().equals(usuario)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void crearGrupo(String nombre, String descripcion, String tipo, String noticias, String owner) {
        Grupo grupo1 = new Grupo(nombre, descripcion, tipo, noticias, owner);
        grupo1.agregarMiembro(buscarUsuario(owner));
        groups.add(grupo1);
        this.agregarGrupoEstudiante(owner, nombre);
    }

    public boolean veriGrupos(String usuario, String grupo){
        Grupo grup = buscarGrupos(grupo);
        
        if(grup!=null){
            for (int i = 0; i < grup.getMiembros().size(); i++) {
                if(grup.getMiembros().get(i).getUsername().equals(usuario)){
                return true;
                }
            }
        }
        return false;
    }
    
    public boolean grupoDuplicado(String nombre) {
        if (!nombre.isEmpty()) {
            for (Grupo grupo : groups) {
                if (grupo.getNombre().equals(nombre)) {
                    return true;
                }
            }
        }
        return false;
    }

    public ArrayList<Usuario> getUsuarios() {
        return users;
    }

    public void setUsuarios(ArrayList<Usuario> users) {
        this.users = users;
    }

    public ArrayList<Grupo> getGrupos() {
        return groups;
    }

    public Grupo buscarGrupos(String grupo) {
        if (!groups.isEmpty()) {
            for (int i = 0; i < groups.size(); i++) {
                if (groups.get(i).getNombre().equals(grupo)) {
                    return groups.get(i);
                }
            }
        }
        return null;
    }
    
    public void setGrupos(ArrayList<Grupo> groups) {
        this.groups = groups;
    }

    public void agregarUsuario(String nombre, String apellido, String usuario, String edad, String clave, String correo) {
        Usuario user = new Usuario(nombre, apellido, usuario, edad, clave, correo);
        users.add(user);
    }
}
