/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author marcu
 */
public class Grupo {

    private String nombre;
    private String descripcion;
    private String tipoGrupo;
    private String noticiasRecientes;
    private String usuario;
    private ArrayList<Usuario> miembros;

    public Grupo() {
    }

    public Grupo(String nombre, String descripcion, String tipoGrupo, String noticiasRecientes, String usuario) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipoGrupo = tipoGrupo;
        this.noticiasRecientes = noticiasRecientes;
        this.usuario = usuario;
        this.miembros = new ArrayList<>();
    }

    public void agregarMiembro(Usuario usuario) {
        miembros.add(usuario);
    }

    public ArrayList<Usuario> getMiembros() {
        return miembros;
    }

    public void setMiembros(ArrayList<Usuario> miembros) {
        this.miembros = miembros;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoGrupo() {
        return tipoGrupo;
    }

    public void setTipoGrupo(String tipoGrupo) {
        this.tipoGrupo = tipoGrupo;
    }

    public String getNoticiasRecientes() {
        return noticiasRecientes;
    }

    public void setNoticiasRecientes(String noticiasRecientes) {
        this.noticiasRecientes = noticiasRecientes;
    }

    public String getPropietario() {
        return usuario;
    }

    public void setPropietario(String usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Grupo{" + "nombre=" + nombre + ", descripcion=" + descripcion + ", tipoGrupo=" + tipoGrupo + ", noticiasRecientes=" + noticiasRecientes + ", usuario=" + usuario + ", miembros=" + miembros + '}';
    }
}
