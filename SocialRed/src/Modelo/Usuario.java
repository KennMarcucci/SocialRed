/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author marcu
 */
public class Usuario {

    private String nombre;
    private String apellido;
    private String edad;
    private String correo;

    private String username;
    private String password;
    
    private ArrayList<Usuario> amigos;
    private ArrayList<Grupo> groups;
    private ArrayList<Publicaciones> publications;
    
    public Usuario() {
    }

    public Usuario(String nombre, String apellido, String edad, String correo, String username, String password) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.correo = correo;
        this.username = username;
        this.password = password;
        
        this.amigos = new ArrayList<>();
        this.groups = new ArrayList<>();
    }

    public void agregarGrupo(Grupo grupo) {
        groups.add(grupo);
    }

    public ArrayList<Grupo> getGrupos() {
        return groups;
    }

    public void agregarAmigo(Usuario amigo) {
        amigos.add(amigo);
    }
    
    public void setArrayListAmigos(ArrayList<Usuario> amigos) {
        this.amigos = amigos;
    }

    public ArrayList<Usuario> getAmigos() {
        return amigos;
    }
    
    public void setGrupos(ArrayList<Grupo> groups) {
        this.groups = groups;
    }

    public String getNombre() {
        return nombre;
    }

    public void editarNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void editarApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEdad() {
        return edad;
    }

    public void editarEdad(String edad) {
        this.edad = edad;
    }

    public String getCorreo() {
        return correo;
    }

    public void editarCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsername() {
        return username;
    }

    public void editarUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void editarPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Usuario{" + "nombre=" + nombre + ", apellido=" + apellido + ", usuario=" + username + ", edad=" + edad + ", clave=" + password + ", correo=" + correo + ", grupos=" + groups + '}';
    }
}
