/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tester;

import Modelo.Principal;

/**
 *
 * @author marcu
 */
public class Test {

    public Principal testiar() {
        Principal principal = new Principal();

        String nombre1 = ("Admin");
        String apellido1 = ("God");
        String edad1 = ("20");
        String correo1 = ("admin@gmail.com");
        String username1 = ("Admin");
        String password1 = ("Admin");

        String nombre2 = ("Virginia");
        String apellido2 = ("Marcucci");
        String edad2 = ("66");
        String correo2 = ("virgi@gmail.com");
        String username2 = ("Virginia");
        String password2 = ("Virginia");

        String nombre3 = ("Wendy");
        String apellido3 = ("Monsalve");
        String edad3 = ("20");
        String correo3 = ("wendy@gmail.com");
        String username3 = ("Wendy");
        String password3 = ("Wendy");

        String nombre4 = ("Diego");
        String apellido4 = ("Trigos");
        String edad4 = ("29");
        String correo4 = ("diego@gmail.com");
        String username4 = ("Diego");
        String password4 = ("Diego");

        String nombre5 = ("Amparo");
        String apellido5 = ("Daza");
        String edad5 = ("90");
        String correo5 = ("amparo@gmail.com");
        String username5 = ("Amparo");
        String password5 = ("Amparo");

        String nombre6 = ("Kenn");
        String apellido6 = ("Marcucci");
        String edad6 = ("21");
        String correo6 = ("kenn@gmail.com");
        String username6 = ("Kenn");
        String password6 = ("Kenn");

        String nombre7 = ("Bad Bunny");
        String apellido7 = ("Martinez");
        String edad7 = ("30");
        String correo7 = ("benito@gmail.com");
        String username7 = ("BadBunny");
        String password7 = ("BadBunny");

        String nombre8 = ("Maria Elena");
        String apellido8 = ("Cardenas");
        String edad8 = ("25");
        String correo8 = ("maria@gmail.com");
        String username8 = ("Maria");
        String password8 = ("Maria");

        String nombre9 = ("Laura Sofia");
        String apellido9 = ("Colmenares");
        String edad9 = ("33");
        String correo9 = ("laura@gmail.com");
        String username9 = ("Laura");
        String password9 = ("Laura");

        String nombre10 = ("Jose Daniel");
        String apellido10 = ("Castellanos");
        String edad10 = ("100");
        String correo10 = ("jose@gmail.com");
        String username10 = ("Jose");
        String password10 = ("Jose");

        principal.agregarUsuario(nombre1, apellido1, edad1, correo1, username1, password1);
        principal.agregarUsuario(nombre2, apellido2, edad2, correo2, username2, password2);
        principal.agregarUsuario(nombre3, apellido3, edad3, correo3, username3, password3);
        principal.agregarUsuario(nombre4, apellido4, edad4, correo4, username4, password4);
        principal.agregarUsuario(nombre5, apellido5, edad5, correo5, username5, password5);
        principal.agregarUsuario(nombre6, apellido6, edad6, correo6, username6, password6);
        principal.agregarUsuario(nombre7, apellido7, edad7, correo7, username7, password7);
        principal.agregarUsuario(nombre8, apellido8, edad8, correo8, username8, password8);
        principal.agregarUsuario(nombre9, apellido9, edad9, correo9, username9, password9);
        principal.agregarUsuario(nombre10, apellido10, edad10, correo10, username10, password10);
        
        return principal;
    }
}
